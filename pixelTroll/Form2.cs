﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pixelTroll123
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            this.Height = Screen.PrimaryScreen.Bounds.Height;
            this.Width = Screen.PrimaryScreen.Bounds.Width*2;
            this.Left = 0;
            this.Top = 0;
        }

        private void Form2_MouseClick(object sender, MouseEventArgs e)
        {
            this.Hide();
        }

        private void Form2_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.Hide();
        }
    }
}
